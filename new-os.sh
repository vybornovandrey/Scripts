#!/bin/bash
echo "*******************************"
echo "*  Begin update new CentOS 7  *"
echo "*******************************"
sleep 5
yum install -y epel-release
yum -y update
yum install -y \
    bind-utils \
    bzip2 \
    gdisk \
    htop \
    iftop \
    nano \
    net-tools \
    screen \
    tmux \
    traceroute \
    tree \
    vim \
    wget \
    yum-utils
echo "*******************************"
echo "*      Disable Firewall       *"
echo "*******************************"
sleep 5
systemctl stop firewalld && systemctl disable firewalld
sleep 5
useradd -c "Andrey V" andrey
usermod -aG wheel andrey
